"""Modulo que ejemplifica el uso de semaforos.
El objetivo es llenar un array de logitud 9
"""

import threading
import time

#inidice global
n = 0
#array global
array = [0]*10
#semaforo
sem = threading.Semaphore()

def fill_array(num):
    """Llena un array de longitud 9 utilizando semaforos"""
    global n
    while True:
        # se toma el semaforo y se reduce a 0
        sem.acquire()
        n = n+1
        # se libera y se incrementa a 1
        sem.release()
        if n > 9:
            break
        #Llena la posicion 'n' el hilo en turno
        array[n] = n
        print ("Hilo {}: {}".format(num,array))
        time.sleep(1)

if __name__ == '__main__':

    """Se crean 3 hilos que en conjunto llenaran el array
    Ejemplo:
        'python semaphore.py'
    """

    t1 = threading.Thread(target = fill_array, args=[1])
    t2 = threading.Thread(target = fill_array, args=[2])
    t3 = threading.Thread(target = fill_array, args=[3])
    t1.start()
    t2.start()
    t3.start()
    t1.join()
    t2.join()
    t3.join()