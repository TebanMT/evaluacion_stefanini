"""Este modulo realiza peticiones de manera concurrente a través de hilos
a un servicio publico de gobierno.

Los datos que se descargan pertenecen a sinaica y son datos de mediciones de CO2.

Es una api con muy poca documentacion y en ocaciones se queda detenidad la coneccion."""

import os
import pandas as pd
import folium
import concurrent.futures
import requests
import threading
import time
import json
import multiprocessing as mp
from glob import glob
from datosgobmx import client


thread_local = threading.local()


def _fetch(pagina):
    """Realiza una peticion al cliente sinaica y guara el resultado en formato json.

    Params
    ----------
        pagina: int
            Numero de a pedir al servicio.
    """
    nombre_archivo = '../data/json/pagina_{}_mediciones.json'.format(pagina)
    if not os.path.exists(nombre_archivo):
        data_api = client.makeCall('sinaica',{'pageSize':1000,'page':pagina})
        print("Leyendo pagina {}".format(pagina))
        with open(nombre_archivo,'w') as f:
            json.dump(data_api, f)

def _all_fetch(paginas, workers=10):
    """Realiza de manera concurrente todas las peticiones.

    Params
    -----------
        paginas: list
            Lista de las paginas a pedir.

        workers: int
            Numero de trabajadore/hilos para realizar las peticiones.
    """
    with concurrent.futures.ThreadPoolExecutor(max_workers=workers) as executor:
        the_futures = [executor.submit(_fetch, p) for p in paginas]
        for future in concurrent.futures.as_completed(the_futures):
            pass


def _json_to_csv(json_file):
    """Convierte un archivo json a uno csv.

    Params
    ----------
        json_file: srt
            Ruta del archivo.
    """
    with open(json_file, 'r') as f:
        results = json.load(f)['results']

    list_data = []
    for r in results:
        aux = pd.DataFrame.from_dict(r,orient='index').T
        list_data.append(aux)
    if len(list_data) > 0:
        list_data = pd.concat(list_data, ignore_index=True)
        return list_data
    else:
        print("No existen datos en {}".format(json_file))
        return []

def parse_all_json_to_csv_parallel(num_paginas, num_procesadores = 2):
    """Realiza la conversion de todos los archivos json a csv de manera paralela.

    Params
    ----------
        num_paginas: int
            Numero de paginas descargas.

        num_procesadores: int, default 2
            Numero de procesadores para realizar el trabajo.
    """
    nombre_file = "../data/json/pagina_{}_mediciones.json"
    c_paginas = 1
    all_datos = []
    for i, j in zip([1,int(num_paginas/2)],[int(num_paginas/2), num_paginas]):
        print("Procesando Bloque {} ".format(c_paginas))
        aux_list = [nombre_file.format(n) for n in range(i,j)]
        with mp.Pool(processes = num_procesadores) as pool:
            sinaica_mediciones = pool.map(_json_to_csv, aux_list)
        sinaica_mediciones = pd.concat(sinaica_mediciones,ignore_index=True)
        sinaica_mediciones.to_csv("../data/csv/pagina_{}_{}_mediciones.csv".format(i,j), index=False)
        c_paginas = c_paginas + 1


def main(num_paginas, num_procesadores=2):
    """Controla la ejecucion principal del programa. La logica es, primero descargar
    los datos enformato json y posteriormente convertirlos en csv.

    Params
    ----------
        num_paginas: int
            Numero de paginas descargas.

        num_procesadores: int, default 2
            Numero de procesadores para realizar el trabajo.
    """
    if num_paginas >= 3200:
        raise ValueError("Numero de paginas excede el limite")
    peticiones = [i+1 for i in range(num_paginas)]
    print("Descargando Datos")
    _all_fetch(peticiones)
    print("Convirtiendo datos a csv")
    parse_all_json_to_csv_parallel(num_paginas, num_procesadores)


if __name__ == '__main__':
    """Ejecucion por consola.
    Parameters
    ----------
        paginas: int, default 10
            Numero de paginas a descargar.

        procesadores: int, default 2
            Numero de procesadores.

    Ejemplo
    ----------
        'python concurrency.py --paginas=10 --procesadores=4'
    """

    import getopt
    import sys

    paginas = 10
    procesadores = 2

    try:
        options, remainder = getopt.getopt(
            sys.argv[1:],
            'p:w',
            ['paginas=',
            'procesadores=',]
        )
    except getopt.GetoptError as err:
        print('ERROR:', err)
        sys.exit(1)

    for opt, arg in options:
        if opt in ('-p', '--paginas'):
            paginas = int(arg)
        if opt in ('-w', '--procesadores'):
            procesadores = int(arg)
    main(paginas,procesadores)