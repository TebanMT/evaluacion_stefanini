"""Pruebas Unitarias. Aun necesitan más covertura"""

import unittest
import mock
import numpy as np
import multiprocessing as mp

import paralellism as p

class mock_struct(object):

    def __init__(self, *args, **kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)
    def get(self, k, default=None):
        try:
            return getattr(self,k)
        except AttributeError:
            return default

class mock_item(mock_struct):
    pass

class MochilaTests(unittest.TestCase):

    def setUp(self):
        self.manager = mp.Manager()
        self.return_dict = self.manager.dict()
        self.items = [
            mock_item(id=0, valor_item=1, peso_item=1),
            mock_item(id=1, valor_item=2, peso_item=2),
            mock_item(id=2, valor_item=3, peso_item=3),
            mock_item(id=3, valor_item=4, peso_item=4),
            mock_item(id=4, valor_item=5, peso_item=5)
        ]

    def test_initial_solution(self):
        m = p.Mochila(10, self.items, 100)
        r = m.initial_solution()
        self.assertEqual(len(r),5)

    def test_random_neighbor(self):
        m = p.Mochila(1, self.items, 100)
        d = {"binary":[0,0,0,0,0], "obj":[]}
        r = m.random_neighbor(d)

    @mock.patch('paralellism.Mochila.initial_solution')
    def test_solve(self, mock_solucion):

        m = p.Mochila(12, self.items, 10)
        mock_solucion.return_value = {"binary":[0,0,0,0,1], "obj":[]}
        m.solve_rmhc(self.return_dict,False)


if __name__ == '__main__':
    unittest.main()