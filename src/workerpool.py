"""Modulo para el algoritmo Genetico.
Utiliza 'trabajadores' en paralelo cuando se crea la poblacion inicial
"""

import multiprocessing as mp
import numpy as np
import time
import random
import copy
from datetime import datetime

class Individuo:
    """Classe Individuo contiene los atributos de un individuo de la poblacion"""

    def __init__(self, num_genes, limite_sup=None, limite_inf=None, repre="discreta", verbose = False):
        """Genera un individuo con sus atributos

        Parameters
        ----------
        num_genes : int
            numero de genes, todos tiene la misma cantidad

        limite_sup : float
            limite superior delimitado por el problema

        limite_sup : float
            limite inferior delimitado por el problema

        repre: srt, default 'discreta'
            representacion de la sulucion, puede ser 'discreta' o 'real'

        verbose: boolean
            Imprimir por pantalla
        """
        self.num_genes = num_genes
        self.fitness = None
        self.valor_funcion = None
        if limite_inf is isinstance(limite_inf,list) and limite_sup is isinstance(limite_sup,list):
            if len(limite_sup) != num_genes or len(limite_inf) != num_genes:
                raise Exception("Los limites deben corresponder a la cantidad de genes del individuo")
            else:
                self.limites_sup = limite_sup
                self.limites_inf = limite_inf
        else:
            self.limites_sup = [limite_sup for _ in range(num_genes)]
            self.limites_inf = [limite_inf for _ in range(num_genes)]
        if repre == "discreta":
            self.cromosoma = [random.randint(self.limites_inf[i],self.limites_sup[i]) for i in range(num_genes)]
        elif repre == "real":
            self.cromosoma = [random.uniform(self.limites_inf[i],self.limites_sup[i]) for i in range(num_genes)]
        else:
            self.cromosoma = repre()

    def calcular_fitness(self, funcion_objetivo, optimizacion="minimizar", verbose=False):
        """Calcula el ajuste de la solucion.

        Parameters
        ----------
        funcion_objetivo : func
            funcion a optimizar.

        optimizacion: str, default 'minimizar'
            'minimizar' o 'maximizar'

        verbose: boolean
            Imprimir por pantalla
        """

        if not optimizacion in ["maximizar", "minimizar"]:
            raise Exception( "El argumento optimizacion debe ser: 'maximizar' o 'minimizar'")
        self.valor_funcion = funcion_objetivo(*self.cromosoma)
        if optimizacion == "maximizar":
            self.fitness = self.valor_funcion
        elif optimizacion == "minimizar":
            self.fitness = (-self.valor_funcion)

    def mutar(self, prob_mutar = 0.01, tipo="bit-flip", verbose=False):
        """Muta el cromosoma de un individuo.
        Parameters
        ----------
        prob_mutar : float, default 0.01
            probabiliad de mutar, se recomienda un numero pequeño para tener mejores resultados.

        tipo: str, default 'bit-flip'
            metodo de mutacion. Puede ser: '"bit-flip', 'intercambio', 'uniforme'

        verbose: boolean
            Imprimir por pantalla
        """
        prob = np.random.uniform(low=0, high=1, size=self.num_genes)
        prob = prob_mutar > prob
        if tipo=="bit-flip":
            # Mutacion Bit-flip
            self.cromosoma = [1 ^ self.cromosoma[i] if v else self.cromosoma[i] for i,v in  enumerate(prob)]
        elif tipo == "intercambio":
            #Mutacion de Intercambio
            if prob_mutar > float(prob[0]):
                random_index = np.random.randint(len(self.cromosoma), size=2)
                self.cromosoma[random_index[0]], self.cromosoma[random_index[1]] = self.cromosoma[random_index[1]], self.cromosoma[random_index[0]]
        elif tipo == "uniforme":
            factor_mutacion = np.random.uniform(low=-1,high=1,size=self.num_genes)
            self.cromosoma = [self.cromosoma[i]+factor_mutacion[i] if v else self.cromosoma[i] for i,v in enumerate(prob)]

        for i in range(self.num_genes):
            if self.cromosoma[i] < self.limites_inf[i]:
                self.cromosoma[i] = self.limites_inf[i]
            if self.cromosoma[i] > self.limites_sup[i]:
                self.cromosoma[i] = self.limites_sup[i]
        self.valor_funcion = None
        self.fitness = None

    def __repr__(self):
        texto = "Individuo" \
                + "\n" \
                + "---------" \
                + "\n" \
                + "Cromosoma: " + str(self.cromosoma) \
                + "\n" \
                + "Valor función objetivo: " + str(self.valor_funcion) \
                + "\n" \
                + "Fitness: " + str(self.fitness) \
                + "\n" \
                + "Límites inferiores de cada variable: " \
                + str(self.limites_inf) \
                + "\n" \
                + "Límites superiores de cada variable: " \
                + str(self.limites_sup) \
                + "\n"
        return(texto)


class Poblacion:
    """Classe Poblacion, representa una poblacion de indiviuos"""

    def __init__(self, num_individuos, num_genes, limites_inf=None, limites_sup=None, repre="discreta", verbose = False, num_procesadores=4):
        """Construye una poblacion. Este proceso se realiza en paralelo con un pool de trabajadores.

        Parameters
        ----------
        num_individuos : int
            Numero de indiviuos en la poblacion

        num_genes : int
            Numero de genes de cada individuo

        limite_inf: float
            limite inferior delimitado por el problema

        limite_sup : float
            limite superior delimitado por el problema

        repre: srt, default 'discreta'
            representacion de la sulucion, puede ser 'discreta' o 'real'

        num_procesadores: int, default 4
            Numero de procesos para crear la problacion, no superar los diponibles por el computador

        verbose: boolean
            Imprimir por pantalla
        """
        self.num_individuos = num_individuos
        self.num_genes = num_genes
        self.limites_inf = limites_inf
        self.limites_sup = limites_sup
        self.optimizado = False
        self.iter_optimizacion = None
        self.tiempo_optimizacion = None
        self.mejor_individuo = None
        self.mejor_fitness = None
        self.mejor_valor_funcion = None
        self.mejor_cromosoma = None
        self.historico_individuos = []
        self.historico_mejor_cromosomas = []
        self.historico_mejor_fitness = []
        self.historico_mejor_valor_funcion = []
        self.diferencia_abs = []
        self.num_procesadores = num_procesadores
        # data.frame con la información del mejor fitness y valor de variables
        # encontrado en cada generación, así como la diferencia respecto a la
        # generación anterior.
        self.resultados_df = None
        self.fitness_optimo = None
        self.cromosoma_optimo = None
        self.valor_funcion_optimo = None
        with mp.Pool(processes=self.num_procesadores) as pool:
            self.individuos = pool.starmap(Individuo, [(num_genes,limites_sup,limites_inf,repre) for _ in range(num_individuos)])

        if verbose:
            print("----------------")
            print("Población creada")
            print("----------------")
            print("Número de individuos: " + str(self.num_individuos))
            print("Límites inferiores de cada variable: " \
                  + str(self.limites_inf))
            print("Límites superiores de cada variable: " \
                  + str(self.limites_sup))
            print("")

    def imprimir_individuos(self, n=None):
        """Imprime 'n' cantidad de indiviuos. Funcion unicamente para fines de visualización.
        Parameters
        ----------
        n : int
            cantidad de individuos a imprimir
        """
        if n is None:
            n = self.num_individuos
        elif n > self.num_individuos:
            n = self.num_individuos
        for i in range(n):
            print(self.individuos[i])

    def evaluar_poblacion(self, funcion_objetivo, optimizacion='minimizar', verbose = False):
        """ Evalua el ajuste de una poblacion con respecto a la funcion objetivo.
    
        Parameters
        ----------
        funcion_objetivo : func
            funcion a optimizar.

        optimizacion: str, default 'minimizar'
            'minimizar' o 'maximizar'

        verbose: boolean
            Imprimir por pantalla
        """
        for i in self.individuos:
            i.calcular_fitness(
                funcion_objetivo = funcion_objetivo,
                optimizacion = optimizacion,
                verbose = verbose
            )
        self.mejor_individuo = copy.deepcopy(self.individuos[0])
        for i in self.individuos:
            if i.fitness > self.mejor_individuo.fitness:
                self.mejor_individuo = i
        self.mejor_fitness = self.mejor_individuo.fitness
        self.mejor_valor_funcion = self.mejor_individuo.valor_funcion
        self.mejor_cromosoma = self.mejor_individuo.cromosoma
        if verbose:
            print("------------------")
            print("Población evaluada")
            print("------------------")
            print("Mejor fitness encontrado : " + str(self.mejor_fitness))
            print("Valor de la función objetivo: " \
                + str(self.mejor_valor_funcion))
            print("Mejor cromosoma encontrado : "
                + str(self.mejor_cromosoma))
            print("")

    def seleccionar_individuos(self, n, return_indices=False, metodo_seleccion="tournament", verbose=False):
        """Selecciona individuos de una poblacion dado un determinado proceso.

        Parameters
        ----------
        n : int
            Numero de individuos a seleccionar.

        return_indices: boolean, default False
            para retornar los indices de los individuos seleccionados.

        metodo_seleccion: srt, default 'tournament'
            Metodo de selccion de los individuos. Puede ser: 'tournament', 'ruleta', 'rank'

        verbose: boolean
            Imprimir por pantalla

        Return
        ------------
            individuos: list
                Copia de individuos seleccionados
        """
        if metodo_seleccion not in ["ruleta", "rank", "tournament"]:
            raise Exception( "El método de selección debe de ser ruleta, rank o tournament")
        array_fitness = [copy.copy(i.fitness) for i in self.individuos]
        if metodo_seleccion == "ruleta":
            prob_selec = array_fitness / np.sum(array_fitness)
            index_seleccionado = np.random.choice(
                a    = np.arange(self.individuos),
                size = n,
                p    = list(prob_selec),
                replace = True
            )
        elif metodo_seleccion == "rank":
            order = np.flip(np.argsort(a=array_fitness) + 1)
            ranks = np.argsort(order) + 1
            probabilidad_seleccion = 1 / ranks
            probabilidad_seleccion = probabilidad_seleccion / np.sum(probabilidad_seleccion)
            index_seleccionado = np.random.choice(
                                a       = np.arange(self.num_individuos),
                                size    = n,
                                p       = list(probabilidad_seleccion),
                                replace = True
                            )
        elif metodo_seleccion == "tournament":
            index_seleccionado = []
            for _ in range(n):
                c_a = np.random.choice(a=np.arange(self.num_individuos),size=2,replace=False)
                c_b = np.random.choice(a=np.arange(self.num_individuos),size=2,replace=False)
                if array_fitness[c_a[0]] > array_fitness[c_a[1]]:
                    ganador_a = c_a[0]
                else:
                    ganador_a = c_a[1]

                if array_fitness[c_b[0]] > array_fitness[c_b[1]]:
                    ganador_b = c_b[0]
                else:
                    ganador_b = c_b[1]

                if array_fitness[ganador_a] > array_fitness[ganador_b]:
                    ind_final = ganador_a
                else:
                    ind_final = ganador_b

                index_seleccionado.append(ind_final)
        if verbose:
            print("----------------------")
            print("Individuo seleccionado")
            print("----------------------")
            print("Método selección: " + metodo_seleccion)
            print("")

        if(return_indices):
            return(index_seleccionado)
        else:
            if n == 1:
                return(copy.deepcopy(self.individuos[int(index_seleccionado)]))
            if n > 1:
                return(
                    [copy.deepcopy(self.individuos[i]) for i in index_seleccionado]
                )

    def cruzamiento(self, padre_1, padre_2, cruzamiento="un_corte", verbose=False):
        """Genera un par de nuevos individuos.

        Parameters
        ----------
        padre_1 : Individuo
            Padre numero 1 de los nuevos individuos

        padre_2 : Individuo
            Padre numero 1 de los nuevos individuos

        cruzamiento: srt, default 'un_corte'
            Tipo de metodo para el cruzamiento. Puede ser: 'un_corte', 'dos_corte' o 'uniforme'

        verbose: boolean
            Imprimir por pantalla

        Return
        ------------
            individuos: list
                Copia de individuos generados
        """
        cromosoma_1, cromosoma_2 = [], []
        if cruzamiento == "un_corte":
            # Cruzamiento de un Punto de corte
            punto_cruce = np.random.randint(self.num_genes, size=1)[0]
            cromosoma_1 = padre_1.cromosoma[:punto_cruce] + padre_2.cromosoma[punto_cruce:]
            cromosoma_2 = padre_2.cromosoma[:punto_cruce] + padre_1.cromosoma[punto_cruce:]
        elif cruzamiento == "dos_cortes":
            # Cruzamiento de Dos Puntos de Corte
            puntos_cruce = np.random.randint(self.num_genes, size=2)
            punto_min , punto_max = min(puntos_cruce) , max(puntos_cruce)
            cromosoma_1 = padre_1.cromosoma[:punto_min] + padre_2.cromosoma[punto_min:punto_max] + padre_1.cromosoma[punto_max:]
            cromosoma_2 = padre_2.cromosoma[:punto_min] + padre_1.cromosoma[punto_min:punto_max] + padre_2.cromosoma[punto_max:]
        elif cruzamiento == "uniforme":
            # Cruzamiento uniforme
            mask = np.random.choice(a = [True, False], size = self.num_genes, replace = True)
            for i,v in enumerate(mask):
                if v:
                    cromosoma_1.append(padre_1.cromosoma[i])
                    cromosoma_2.append(padre_2.cromosoma[i])
                else:
                    cromosoma_1.append(padre_2.cromosoma[i])
                    cromosoma_2.append(padre_1.cromosoma[i])
        elif cruzamiento.upper() == "OX1":
            #Davis Order Crossover
            cromosoma_1 = [None for _ in padre_1.cromosoma]
            cromosoma_2 = [None for _ in padre_2.cromosoma]
            puntos_cruce = np.random.randint(self.num_genes, size=2)
            punto_min , punto_max = min(puntos_cruce) , max(puntos_cruce)
            cromosoma_1[punto_min:punto_max] = padre_1.cromosoma[punto_min:punto_max]
            cromosoma_2[punto_min:punto_max] = padre_2.cromosoma[punto_min:punto_max]
            for i,v in enumerate(padre_2.cromosoma):
                if v not in cromosoma_1:
                    cromosoma_1[i] = padre_2.cromosoma[i]
            for i,v in enumerate(padre_1.cromosoma):
                if v not in cromosoma_2:
                    cromosoma_2[i] = padre_1.cromosoma[i]
        hijo_1 = copy.deepcopy(padre_1)
        hijo_1.cromosoma = cromosoma_1
        hijo_2 = copy.deepcopy(padre_2)
        hijo_2.cromosoma = cromosoma_2
        return [hijo_1, hijo_2]

    def reemplazo_generacion(self, metodo_seleccion="tournament", prob_mutar=0.01, tipo_mutacion="bit-flip",
                            elitismo=0.0, verbose_seleccion=False, verbose_cruce=False, cruzamiento = "uniforme",
                            verbose_mutacion=False, verbose=False):
        """Genera una nueva generación de individuos dado un cierto criterio.

        Parameters
        ----------
        metodo_seleccion : str, default 'tournament'
            Metodo para seleccionar individuos.

        prob_mutar : float, default 0.01
            Parobabilidad de mutar

        tipo_mutacion: srt, default 'bit-flip'
            Metodo de mutacion.

        elitismo: float, default 0.0
            Determina si pasan individuos directamente a una nueva generacion sin import su ajuste.

        verbose: boolean
            Imprimir por pantalla
        """
        nueva_generacion = []
        if elitismo > 0:
            # Número de individuos que pasan directamente a la siguiente
            # generación.
            n_elitismo = int(np.ceil(self.num_individuos*elitismo))

            # Se identifican los n_elitismo individuos con mayor fitness (élite).
            array_fitness = [copy.copy(i.fitness) for i in self.individuos]
            rank = np.flip(np.argsort(array_fitness))
            elite = [copy.deepcopy(self.individuos[i]) for i in rank[:n_elitismo]]
            # Se añaden los individuos élite a la lista de nuevos individuos.
            nueva_generacion = nueva_generacion + elite
        else:
            n_elitismo = 0

        while len(nueva_generacion)-n_elitismo < self.num_individuos:
            padres = self.seleccionar_individuos(n = 2, return_indices = False, metodo_seleccion = metodo_seleccion, verbose = verbose_seleccion)
            descendencia = self.cruzamiento(padre_1 = padres[0], padre_2 = padres[1],cruzamiento=cruzamiento,verbose = verbose_cruce)
            for d in descendencia:
                d.mutar(
                    prob_mutar  = prob_mutar,
                    tipo        = tipo_mutacion,
                    verbose          = verbose_mutacion
                )
            nueva_generacion = nueva_generacion + [descendencia[0]]
            if len(nueva_generacion) >= self.num_individuos:
                break
            nueva_generacion = nueva_generacion + [descendencia[1]]

        self.individuos = copy.deepcopy(nueva_generacion)
        self.mejor_individuo = None
        self.mejor_fitness = None
        self.mejor_valor_variables = None
        self.mejor_valor_funcion = None
        # INFORMACIÓN DEL PROCESO (VERBOSE)
        # ----------------------------------------------------------------------
        if verbose:
            print("-----------------------")
            print("Nueva generación creada")
            print("-----------------------")
            print("Método selección: " + metodo_seleccion)
            print("Elitismo: " + str(elitismo))
            print("Número individuos élite: " + str(n_elitismo))
            print("Número de nuevos individuos: "\
                + str(self.num_individuos-n_elitismo))
            print("")

    def optimizar(self, funcion_objetivo, num_generaciones=10, optimizacion="maximizar",
                  metodo_seleccion="tournament", elitismo = 0.0, prob_mutar= 0.01, cruzamiento = "uniforme",
                  tipo_mutacion="bit-flip", rondas_tolerancia=10, stop_temprano = False,
                  tolerancia_parada = 0.1, verbose_evaluacion=False, verbose_nueva_gen=False,
                  verbose_seleccion=False, verbose_cruce=False, verbose_mutacion=False, verbose=False):
        """Optimiza una poblacion. Funcion principal."""
        start_time = time.time()
        for i in range(num_generaciones):
            if verbose:
                print("-------------")
                print("Generación: " + str(i))
                print("-------------")
            self.evaluar_poblacion(funcion_objetivo=funcion_objetivo, optimizacion=optimizacion, verbose=verbose_evaluacion)
            # Almacenar el historico
            self.historico_individuos.append(copy.deepcopy(self.individuos))
            self.historico_mejor_fitness.append(copy.deepcopy(self.mejor_fitness))
            self.historico_mejor_cromosomas.append(copy.deepcopy(self.mejor_cromosoma))
            self.historico_mejor_valor_funcion.append(copy.deepcopy(self.mejor_valor_funcion))
            #calculo de la diferencia absoluta respecto a la generacion anterior
            if i == 0:
                self.diferencia_abs.append(None)
            else:
                diferencia = abs(self.historico_mejor_fitness[i]/-self.historico_mejor_fitness[i-1])
                self.diferencia_abs.append(diferencia)
            #Criterio de Parada
            # Ocurre cuando la diferencia adsoluta entre el mejor individuo de cada generacion
            # no supera el valor de tolerancia de parada en las ultimas n generaciones
            # el valor de las n generaciones esta dado por el paramentro rondas_tolerancia
            if stop_temprano and i > rondas_tolerancia:
                ultimos_n_mejor_fitness = np.array(self.diferencia_abs[-(rondas_tolerancia):])
                if all(ultimos_n_mejor_fitness < tolerancia_parada):
                    if verbose:
                        print("Algoritmo detenido en la generación "
                            + str(i) \
                            + " por falta cambio absoluto mínimo de " \
                            + str(tolerancia_parada) \
                            + " durante " \
                            + str(rondas_tolerancia) \
                            + " generaciones consecutivas.")
                    break
            #Crear una nueva generacion
            self.reemplazo_generacion(
                metodo_seleccion=metodo_seleccion,
                elitismo=elitismo,
                prob_mutar=prob_mutar,
                tipo_mutacion = tipo_mutacion,
                cruzamiento = cruzamiento,
                verbose=verbose_nueva_gen,
                verbose_seleccion=verbose_seleccion,
                verbose_cruce=verbose_cruce,
                verbose_mutacion=verbose_mutacion)
        end_time = time.time()
        self.optimizado = True
        self.iter_optimizacion = i+1
        self.tiempo_optimizacion = end_time - start_time
        # Datos del mejor individuo de todas las generaciones
        self.fitness_optimo = max(self.historico_mejor_fitness)
        self.valor_funcion_optimo = max(self.historico_mejor_valor_funcion)
        self.cromosoma_optimo = self.historico_mejor_cromosomas[self.historico_mejor_fitness.index(self.fitness_optimo)]
        #Resumen
        if verbose:
            print("-------------------------------------------")
            print("Optimización finalizada " \
                  + datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            print("-------------------------------------------")
            print("Duración optimización: " + str(end_time - start_time))
            print("Número de generaciones: " + str(self.iter_optimizacion))
            print("Cromosoma Optimo: " + str(self.cromosoma_optimo))
            print("Valor función objetivo: " + str(self.valor_funcion_optimo))
            print("")

    def __repr__(self):
        texto = "============================" \
                + "\n" \
                + "         Población" \
                + "\n" \
                + "============================" \
                + "\n" \
                + "Número de individuos: " + str(self.num_individuos) \
                + "\n" \
                + "Límites inferiores de cada variable: " + str(self.limites_inf) \
                + "\n" \
                + "Límites superiores de cada variable: " + str(self.limites_sup) \
                + "\n" \
                + "Optimizado: " + str(self.optimizado) \
                + "\n" \
                + "Iteraciones optimización (generaciones): " \
                     + str(self.iter_optimizacion) \
                + "\n" \
                + "\n" \
                + "Información del mejor individuo:" \
                + "\n" \
                + "----------------------------" \
                + "\n" \
                + "Cromosoma: " + str(self.mejor_cromosoma) \
                + "\n" \
                + "Fitness: " + str(self.mejor_fitness) \
                + "\n" \
                + "\n" \
                + "Resultados tras optimizar:" \
                + "\n" \
                + "--------------------------" \
                + "\n" \
                + "Cromosoma óptimo: " + str(self.cromosoma_optimo) \
                + "\n" \
                + "Valor óptimo función objetivo: " + str(self.valor_funcion_optimo) \
                + "\n" \
                + "Fitness óptimo: " + str(self.fitness_optimo)
        return(texto)

class Funciones():
    """Clase para funciones que requieran ser optimizadas"""

    def __init__(self, max_iter, intervalo, D, function):
        """Construye la los atributos asociados a una funcion.

        Parameters
        ----------
        max_iter : int
           Numero maximo de Iteraciones para optimizar.

        intervalo: tuple
            El primer elemento es el intervalo inferior y el segundo el inferior.

        D: int
            Numero de dimensiones de la solucion.

        funcion: func
            Funcion objetivo
        """
        self.max_iter = max_iter
        self.intervalo = intervalo
        self.D = D
        self.function = function

    def initial_solution(self):
        """Solucion inicial. Es una solucion aleatoria que se mantiene dentro de los limites definidos."""
        res = [np.round(np.random.uniform(low=self.intervalo[0], high=self.intervalo[1]),5) for _ in range(self.D)]
        return res

    def solve_ga(self,num_individuos, num_generaciones=100):
        """Ejecuta la optimizacion por algoritmo genetico

        Params
        ----------
        num_individuos: int
            Numero de individuos de la poblacion

        num_generaciones: int, default 100
            Numero de generaciones.

        Return
        ------------
        result: list
            Lista con dos elementos. El primero es el cromosoma con mejor ajuste y el segundo el valor de ajuste
        """
        def evaluacion(*solution):
            def __getitem__(array, key):
                try:
                    return array[key]
                except IndexError:
                    return 0
            res = eval(self.function,{"__getitem__":__getitem__,"solution":list(solution),"np":np})
            return res
        poblacion = Poblacion(num_individuos, self.D, limites_inf=self.intervalo[0], limites_sup=self.intervalo[1], repre=self.initial_solution, verbose=False)
        poblacion.optimizar(evaluacion, optimizacion="minimizar", num_generaciones=num_generaciones, tipo_mutacion="uniforme", cruzamiento="uniforme", elitismo=0.2, stop_temprano=True, rondas_tolerancia=10)
        return [poblacion.cromosoma_optimo,poblacion.valor_funcion_optimo]



def execute(functions):
    """Optimiza una función o funciónes.
    
    Params
    ----------
    functions: func
        Funcion a optimizar
    """
    import time
    for f in functions:
        h = Funciones(500, [-10,10],1, f)
        statistic = []
        for _ in range(2):
            start_time = time.time()
            s = h.solve_ga(50,100)
            finish_time = time.time()
            statistic.append((s[1], finish_time - start_time))
        yield statistic

if __name__ == '__main__':

    """Ejecucion por consola.
    Parameters
    ----------
        funcion: int, default 0
            Indice de la funcion a ejecutar. El indice debe corresponder al catalogo de funciones.

    Ejemplo
    ----------
        'python workerpool.py --funcion=1'
    """

    import getopt
    import sys

    indice = 0

    try:
        options, remainder = getopt.getopt(
            sys.argv[1:],
            'f',
            ['funcion=',]
        )
    except getopt.GetoptError as err:
        print('ERROR:', err)
        sys.exit(1)

    for opt, arg in options:
        if opt in ('-f', '--funcion'):
            indice = int(arg)


    functions = [
    #|𝑥𝑖sin(𝑥𝑖)+0.1𝑥𝑖|
    "sum(map(lambda x: abs(x*np.sin(x)+0.1*x),solution))",
    #(𝑥1−1)^2+  𝑖(2sin(𝑥𝑖)−𝑥𝑖−1)^2
    "(solution[0]-1)**2 + sum(map(lambda args: args[0]*(2*np.sin(args[1])-solution[args[0]-1])**2, enumerate(solution[1:])))",
    #|𝑥𝑖^5−3𝑥𝑖^4+4𝑥𝑖^3−2𝑥𝑖^2−10𝑥_1−4|
    "sum(map(lambda x: abs((x**5)-(3*x**4)+(4*x**3)-(2*x**2)-(10*solution[1])-4), solution))",
    #x^10
    "sum(map(lambda x: x**10, solution))",
    #
    "sum(map(lambda args: (__getitem__(solution,args[0]+1)**2 + args[1]**2)**0.25 * (np.sin(50*(__getitem__(solution,args[0]+1)**2+args[1]**2)**0.1)**2 +0.1), enumerate(solution)))",
    #ix^2
    "sum(map(lambda args: args[0]*args[1]**2, enumerate(solution)))"
    ]
    h = Funciones(500, [-10,10],1, functions[indice])
    s = h.solve_ga(50,100)
    print(s)