"""Web Scrapping asincrono.
El objetivo es buscar en wikipedia la fecha de Nacimiento de cientificos historicos.
"""

import aiohttp
from bs4 import BeautifulSoup
import pandas as pd
import requests
import nest_asyncio
import asyncio
import re

nest_asyncio.apply()

#Exprecion regular para buscar fecha de Nacimiento
is_fecha_nacimiento = lambda x: re.search('[0-9]{1,2}\s*de\s*\w*\s*de\s*[0-9]{4}', x)

class WebScraper(object):
    """Classe WebScraper, contiene la logica asincrona para realizar web scraping"""
    def __init__(self, urls):
        """Contruye un Scraper.

        Params
        ---------
            urls: dict
                Diccionario de urls a buscar. La llave es el nombre del cientifico y el valor la url de wikipedia"""
        self.urls = urls
        self.all_data  = []
        self.master_dict = {}
        # Run The Scraper:
        asyncio.run(self.main())

    async def fetch(self, session, url, nombre):
        """Realiza la consulta a la url.

        Params
        -----------
        session: session
            session de aiohttp con la que se realizan las consultas.

        url: str
            Url a consultar.

        nombre: str
            Nombre del cientifico.

        Return
        -----------
            text,url,fecha,nombre: tuple
                volcado html de la pagina, url, fecha de Nacimiento y nombre del cientifico
        """
        try:
            async with session.get(url) as response:
                text = await response.text()
                fecha = await self.extract_fecha_nacimeinto(text)
                return text, url, fecha, nombre
        except Exception as e:
            print(str(e))

    async def extract_fecha_nacimeinto(self, text):
        """Obtiene la fecha de nacimeinto si es posible.

        Params
        ----------
        text: srt
            volcado html

        Returns
        ----------
            Fecha_nacimiento: srt
                Fecha de nacimeinto"""
        try:
            soup = BeautifulSoup(text, 'html.parser')
            ths = soup.find_all('th')
            fecha_nacimiento = []
            for th in ths:
                if th.get_text() == "Nacimiento":
                    fecha_nacimiento.append(is_fecha_nacimiento(th.next_sibling.get_text())[0])
            try:
                return fecha_nacimiento[0]
            except IndexError:
                return "No se encontro la fecha"
        except Exception as e:
            print(str(e))

    async def main(self):
        """Controla la ejecucion principal del scraper."""

        tasks = []
        headers = {
            "user-agent": "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"}
        async with aiohttp.ClientSession(headers=headers) as session:
            for k,url in self.urls.items():
                tasks.append(self.fetch(session, url, k))

            htmls = await asyncio.gather(*tasks)
            self.all_data.extend(htmls)

            # Storing the raw HTML data.
            for html in htmls:
                if html is not None:
                    url = html[1]
                    self.master_dict[html[3]] = {'Raw Html': html[0], 'fecha': html[2]}
                else:
                    continue


if __name__ == '__main__':
    """Ejemplo:
        'python asynchronism.py'
    """
    urls = {
        "Newton":   'https://es.wikipedia.org/wiki/Isaac_Newton',
        "Einstein": "https://es.wikipedia.org/wiki/Albert_Einstein",
        "Arnold Sommerfeld": "https://es.wikipedia.org/wiki/Arnold_Sommerfeld",
        "Dennis Ritchie": "https://es.wikipedia.org/wiki/Dennis_Ritchie",
        "Ken Thompson": "https://es.wikipedia.org/wiki/Ken_Thompson",
        }

    scraper = WebScraper(urls = urls)
    for k,v in urls.items():
        print("La fecha de Nacimiento de {} es el {}".format(k, scraper.master_dict[k]['fecha']))
        print("Fuente: {}".format(v))
        print()