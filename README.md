# Evaluacion Stefanini
En este repositorio se encuentran ejemplos de:
* [Paralellism](http://ferestrepoca.github.io/paradigmas-de-programacion/paralela/paralela_teoria/index.html)
* [Concurrency](http://ferestrepoca.github.io/paradigmas-de-programacion/paralela/paralela_teoria/index.html)
* [Asynchronism](https://realpython.com/async-io-python/)
* [Workerpool](https://docs.python.org/3/library/multiprocessing.html)
* [Semaphore](https://docs.python.org/3/library/threading.html#semaphore-objects)

## Construido con 🛠️

* [Python](https://www.python.org/)

## Requisitos
* [Python](https://www.python.org/) >= 3
* [Cualquier sistema operativo, preferencia Linux](https://getfedora.org/es/)
* [Cualquier arquitectura de Procesador, probado en AMD x64](https://es.wikipedia.org/wiki/X86-64)
* [Virtualenvwrapper (Opicional)](https://virtualenvwrapper.readthedocs.io/en/latest/)

Las depenciencias se encuentran el archivo requeriments.txt

## ¿Cómo configurarlo?
Primero se crea el ambiente virtual de python (opcional).

Utilizando virtualenvwrapper:
```
$ mkvirtualenv 'nombre' --python=python3
```

Por ultimo se instalan las dependencias via pip, dentro del ambiente virtual:
```
$ pip install - r requirements.txt
```

## ¿Qúe modulos contiene?
Dentro de la carpeta src se encuentran los mudulos. Para fines de explicativos, cada modulo tiene como nombre la tecnica paralela que se utilizó dentro del codigo. Aunque en algunos se combinaron más de una tenica.

| Modulo | Descripcion | Ejecucion desde consola |
| ------------- | ------------- | ------------- |
|  ```paralellism.py```  | implementa el paralelismo a nivel Pocesos con la biblioteca multiprocessing. La función del modulo es resolver el problema llamado 'Knaspack problem'. Dicho problema es de complejidad NP, por lo que una de las mejores maneras de solucionarlo es mediante metaheuristicas. En este caso, las metaheuristicas utilizadas son: ascenso de colinas aleatorio, recosido simulado y un algoritmo genetico. El uso del paralelismo está en el hecho de resolver el problema con las 3 metaheuristicas, cada una en un proceso diferente.  | ```python paralellism.py -r=true``` |
| ```asynchronism.py```  | utiliza Web Scrapping asincrono. El objetivo es buscar en wikipedia la fecha de Nacimiento de cientificos historicos. | ``` python asynchronism.py``` |
| ```concurrency.py```  | realiza peticiones de manera concurrente a través de hilos a un servicio publico de gobierno. Los datos que se descargan pertenecen a sinaica y son datos de mediciones de CO2. Es una api con muy poca documentacion y en ocaciones se queda detenidad la conexión.  | ```python concurrency.py --paginas=10 --procesadores=4``` |
| ```workerpool.py```  | implementa el algoritmo Genetico. Utiliza 'trabajadores' en paralelo cuando se crea la poblacion inicial  | ```python workerpool.py --funcion=1``` |
| ```semaphore.py```  | ejemplifica el uso de semaforos. El objetivo es llenar un array de logitud 9.  | ```python semaphore.py``` |
| ```unit.py```  | Pruebas unitarias, falta covertura  | ```python unit.py``` |


Las funciones tambien están disponibles para su ejecucion en algun interprete. Yo recomiendo ```ipython```

